INSERT INTO Greeting (referenceId, text, createdAt, createdBy, updatedAt, updatedBy) values ('2982801005124e9a966e72ad1f5c093b', 'Hello, World!', NOW(), 'user', NULL, NULL);
INSERT INTO Greeting (referenceId, text, createdAt, createdBy, updatedAt, updatedBy) values ('b8e4cc6ff67d47c6a3ba1b25b7ff6567', 'What up, guys?', NOW(), 'user', NULL, NULL);

INSERT INTO Account (referenceId, username, password, enabled, expired, locked, credentialsExpired, version, createdBy, createdAt, updatedBy, updatedAt) 
values ('ECAFA1F8B94C4DC6A0E0B23ACBA2AEE3', 'user', 'user', true, false, false, false, 0, 'user', NOW(), NULL, NULL);

INSERT INTO Account (referenceId, username, password, enabled, expired, locked, credentialsExpired, version, createdBy, createdAt, updatedBy, updatedAt) 
values ('D7AEE52A42BC46C89C51D62794F1A196', 'operations', 'operations', true, false, false, false, 0, 'user', NOW(), NULL, NULL);

INSERT INTO Role (id, code, label, ordinal, effectiveAt, expiresAt, createdAt) values (1, 'ROLE_USER', 'User', 0, '2015-01-01 00:00:00', NULL, NOW());
INSERT INTO Role (id, code, label, ordinal, effectiveAt, expiresAt, createdAt) values (2, 'ROLE_ADMIN', 'Administrator', 0, '2015-01-01 00:00:00', NULL, NOW());
INSERT INTO Role (id, code, label, ordinal, effectiveAt, expiresAt, createdAt) values (3, 'ROLE_SYSADMIN', 'System Administrator', 0, '2015-01-01 00:00:00', NULL, NOW());

INSERT INTO AccountRole (accountId, roleId) Select a.id, r.id from Account a, Role r where a.username = 'user' and r.id = 1;
INSERT INTO AccountRole (accountId, roleId) Select a.id, r.id from Account a, Role r where a.username = 'operations' and r.id = 3;
