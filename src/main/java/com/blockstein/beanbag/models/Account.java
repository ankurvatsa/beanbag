package com.blockstein.beanbag.models;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;

@Entity
public class Account extends TransactionalEntity {
	
	private static final long serialVersionUID = 4L;

	@NotNull
	private String username; 
	
	@NotNull
	private String password;
	
	@NotNull
	private Boolean enabled = true;
	
	@NotNull
	private Boolean expired = false;
	
	@NotNull
	private Boolean locked = false;
	
	@NotNull
	private Boolean credentialsExpired = false;
	
	// An account can have multiple roles, a role will as well be assigned to multiple accounts
	// However, the application will never need to know what accounts have a given role
	@ManyToMany(
			fetch = FetchType.EAGER, 
			cascade = CascadeType.ALL)
	@JoinTable(
			name="AccountRole", 
			joinColumns = @JoinColumn(
					name = "accountId", 
					referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(
					name = "roleId",
					referencedColumnName = "id")
			)
	private Set<Role> roles;
	
	public Account() {
		
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Boolean isExpired() {
		return expired;
	}

	public void setExpired(Boolean expired) {
		this.expired = expired;
	}

	public Boolean isLocked() {
		return locked;
	}

	public void setLocked(Boolean locked) {
		this.locked = locked;
	}

	public Boolean isCredentialsExpired() {
		return credentialsExpired;
	}

	public void setCredentialsExpired(Boolean credentialsExpired) {
		this.credentialsExpired = credentialsExpired;
	}

	public Set<Role> getRoles() {
		return roles;
	}
}
