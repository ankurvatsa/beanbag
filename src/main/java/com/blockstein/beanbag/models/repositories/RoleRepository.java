package com.blockstein.beanbag.models.repositories;

import java.util.Collection;

import org.joda.time.DateTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.blockstein.beanbag.models.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
	
	Collection<Role> findByEffectiveAtBeforeAndExpiresAtAfterOrExpiresAtNullOrderByOrdinalAsc(DateTime effectiveAt, DateTime expiresAt);
	
	@Query("select r from Role r where r.effectiveAt <= :effectiveAt and (r.expiresAt is null or r.expiresAt > :expiresAt) order by r.ordinal asc")
	Collection<Role> findAllEffective(@Param("effectiveAt") DateTime effectiveAt);
	
	Role findByCodeAndEffectiveAtBeforeAndExpiresAtAfterOrExpiresAtNull(String code, DateTime effectiveAt, DateTime expiresAt);
	
	@Query("select r from Role r where r.code = :code and r.effectiveAt <= :effeciveAt and (r.expiresAt is null or r.expiresAt > :effectiveAt)")
	Role findByCodeAndEffective(@Param("code") String code, @Param("effectiveAt") DateTime effectiveAt);
	
}
