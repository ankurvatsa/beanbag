package com.blockstein.beanbag.models.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.blockstein.beanbag.models.Trade;

@Repository
public interface ITradeRepository extends JpaRepository<Trade, Long> {
	
	public List<Trade> findById(Long tradeId);
}
