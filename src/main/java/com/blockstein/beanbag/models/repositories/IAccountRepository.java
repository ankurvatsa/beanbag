package com.blockstein.beanbag.models.repositories;

import com.blockstein.beanbag.models.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IAccountRepository extends JpaRepository<Account, Long> {
	
	Account findByUsername(String username);
	
}
