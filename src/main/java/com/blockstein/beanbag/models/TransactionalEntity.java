package com.blockstein.beanbag.models;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.joda.time.DateTime;

import com.blockstein.beanbag.util.RequestContext;

@MappedSuperclass
public class TransactionalEntity implements Serializable {
	
	private static final long serialVersionUID = 3L;
	
	@Id
	@GeneratedValue
	private Long id;
	
	@NotNull
	private String referenceId = UUID.randomUUID().toString();
	
	@Version
	private Integer version;
	
	@NotNull
	private String createdBy;
	
	@NotNull
	private DateTime createdAt;
	
	private String updatedBy;
	
	private DateTime updatedAt;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public DateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(DateTime createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public DateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(DateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Override
	public int hashCode() {
		if (this.id == null) {
			return -1;
		}
		return this.getId().hashCode();
	}

	@Override
	public boolean equals(Object that) {
		if (that == null) {
			return false;
		}
		
		if (this.getClass().equals(that.getClass())) {
			TransactionalEntity other = (TransactionalEntity) that;
			if((this.getId() == null) || (other.getId() == null)) {
				return false;
			}
			
			if(this.getId().equals(other.getId())) {
				return true;
			}
		}
		return false;
	}
	
	@PrePersist
	public void beforePersist() {
		setCreatedAt(new DateTime());
		
		String username = RequestContext.getUsername();
		if(username == null) {
			throw new IllegalArgumentException("Cannot persist a TransactionEntity without a usernam in the RequestContext for this thread");
		}
		setCreatedBy(username);
	}
	
	@PreUpdate
	public void beforeUpdate() {
		setUpdatedAt(new DateTime());
		
		String username = RequestContext.getUsername();
		if(username == null) {
			throw new IllegalArgumentException("Cannot update a TransactionEntity without a usernmar in the RequestContext for this thread");
		}
		setUpdatedBy(username);
	}
}
