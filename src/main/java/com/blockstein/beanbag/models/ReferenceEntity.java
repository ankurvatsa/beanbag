package com.blockstein.beanbag.models;

import java.io.Serializable;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

import org.joda.time.DateTime;

@MappedSuperclass
public class ReferenceEntity implements Serializable {
	
	private static final long serialVersionUID = 5L;
	
	@Id
	private long id;
	
	@NotNull
	private String code;
	
	@NotNull
	private String label;
	
	@NotNull
	private Integer ordinal;
	
	@NotNull
	private DateTime effectiveAt;
	
	private DateTime expiresAt;
	
	@NotNull
	private DateTime createdAt;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Integer getOrdinal() {
		return ordinal;
	}

	public void setOrdinal(Integer ordinal) {
		this.ordinal = ordinal;
	}

	public DateTime getEffectiveAt() {
		return effectiveAt;
	}

	public void setEffectiveAt(DateTime effectiveAt) {
		this.effectiveAt = effectiveAt;
	}

	public DateTime getExpiresAt() {
		return expiresAt;
	}

	public void setExpiresAt(DateTime expiresAt) {
		this.expiresAt = expiresAt;
	}

	public DateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(DateTime createdAt) {
		this.createdAt = createdAt;
	}
	
}
