package com.blockstein.beanbag.models;
import javax.persistence.Column;
import javax.persistence.Entity;


import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity(name="DTDTradeDetails")
public class Trade extends TransactionalEntity {
	private static final long serialVersionUID = 2L;
	
	public Trade(Trade dbTrade) {
		this.setBuyCcy(dbTrade.getBuyCcy());
		this.setBuyCcyAmt(dbTrade.getBuyCcyAmt());
		this.setSellCcy(dbTrade.getSellCcy());
		this.setSellCcyAmt(dbTrade.getBuyCcyAmt());
		this.setSellToBuyRate(dbTrade.getSellToBuyRate());
		this.setOrderType(dbTrade.getOrderType());
		this.setTradeDirection(dbTrade.getTradeDirection());
		this.setTradeStatus(dbTrade.getTradeStatus());
	}

	public Trade() {
	}

	@JsonIgnore
	public Boolean isActive() {
		return this.tradeStatus == 1;
	}
	
	@JsonIgnore
	public Boolean isValidBuyCcy() {
		return (this.getBuyCcy() > 0) && (this.getBuyCcy() < 100);
	}
	
	@JsonIgnore
	public Boolean isValidSellCcy() {
		return (this.getSellCcy() > 0) && (this.getSellCcy() < 100);
	}
	
	@JsonIgnore
	public Boolean isValidOrderType() {
		return (this.getOrderType() > 0) && (this.getOrderType() < 3);
	}
	
	@JsonIgnore
	public Boolean isValidTradeDirection() {
		return (this.getTradeDirection() > 0) && (this.getTradeDirection() < 3);
	}
	
	public Integer getTradeDirection() {
		return tradeDirection;
	}
	public void setTradeDirection(Integer tradeDirection) {
		this.tradeDirection = tradeDirection;
	}

	public Integer getSellCcy() {
		return sellCcy;
	}
	public void setSellCcy(Integer sellCcy) {
		this.sellCcy = sellCcy;
	}

	public Integer getBuyCcy() {
		return buyCcy;
	}
	public void setBuyCcy(Integer buyCcy) {
		this.buyCcy = buyCcy;
	}

	public Double getBuyCcyAmt() {
		return buyCcyAmt;
	}
	public void setBuyCcyAmt(Double buyCcyAmt) {
		this.buyCcyAmt = buyCcyAmt;
	}

	public Double getSellCcyAmt() {
		return sellCcyAmt;
	}
	public void setSellCcyAmt(Double sellCcyAmt) {
		this.sellCcyAmt = sellCcyAmt;
	}

	public Integer getOrderType() {
		return orderType;
	}
	public void setOrderType(Integer orderType) {
		this.orderType = orderType;
	}

	public Double getSellToBuyRate() {
		return sellToBuyRate;
	}
	public void setSellToBuyRate(Double sellToBuyRate) {
		this.sellToBuyRate = sellToBuyRate;
	}

	public Integer getTradeStatus() {
		return tradeStatus;
	}
	public void setTradeStatus(Integer tradeStatus) {
		this.tradeStatus = tradeStatus;
	}
	
	@Column(name="DTDTradeDirection", nullable=false)
	private Integer tradeDirection;

	@Column(name="DTDSellCcy", nullable=false)
	private Integer sellCcy;
	
	@Column(name="DTDBuyCcy", nullable=false)
	private Integer buyCcy;
	
	@Column(name="DTDBuyCcyAmt", nullable=false)
	private Double buyCcyAmt;
	
	@Column(name="DTDSellCcyAmt", nullable=false)
	private Double sellCcyAmt;
		
	@Column(name="DTDOrderType", nullable=false)
	private Integer orderType;

	@Column(name="DTDSellToBuyRate", nullable=false)
	private Double sellToBuyRate;

	@Column(name="DTDTradeStatus", nullable=false)
	private Integer tradeStatus;

}