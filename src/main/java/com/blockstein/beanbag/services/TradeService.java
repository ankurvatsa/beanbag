package com.blockstein.beanbag.services;

import java.util.ArrayList;
import java.util.List;
import static java.util.stream.Collectors.toList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.blockstein.beanbag.models.Trade;
import com.blockstein.beanbag.models.repositories.ITradeRepository;

@Service
public class TradeService implements ITradeService {
	
	@Autowired
	private ITradeRepository tradeRepository;
	
	public List<Trade> getAllLiveTrades() {
		List<Trade> trades = new ArrayList<>();
		tradeRepository.findAll().forEach(trades::add);
		
		List<Trade> liveTrades = trades.stream().filter(t -> t.isActive()).collect(toList());
		
		return liveTrades;
	}
	
	@Cacheable(value="trades", key="id")
	public Trade getLiveTrade(Long tradeId) {
		List<Trade> tradeList = tradeRepository.findById(tradeId);
		
		List<Trade> liveTrades = tradeList.stream().filter(t -> t.isActive()).collect(toList());
		
		if(liveTrades.size() > 1) {
			throw new RuntimeException("Fatal Error: Multiple live trades found for trade "+tradeId);
		}
		else if(liveTrades.size() == 0) {
			return null;
		}
		return liveTrades.get(0);
	}
	
	public List<Trade> getAllTradeVersions(Long tradeId) {
		return tradeRepository.findById(tradeId);	
	}
	
	@CachePut(value="trades", key="#result.id")
	public Long addTrade(Trade trade) {
		tradeRepository.save(trade);
		return trade.getId();
	}
		
	@CachePut(value="trades", key="id")
	public Long updateTrade(Trade trade, Long tradeId) {
		
		if(trade.getId() != tradeId) {
			throw new RuntimeException("Error: Input trade data is for trade-id " + trade.getId()
					+ " but requested to update trade-id " + tradeId);
		}
		
		Trade dbTrade = getLiveTrade(tradeId);
		if(dbTrade == null) {
			return null;
		}
		
		// Change status of dbTrade to AMENDED and save it to DB
		
		// Save input "trade" to DB 
		
		Trade finalTrade = tradeRepository.save(trade);
		return finalTrade.getId();
	}

	@CacheEvict(value="trades", key="#tradeKey.tradeId")
	public void deleteTrade(Trade trade) {
		tradeRepository.delete(trade);
	}
	
	@Override
	@CacheEvict(value="trades", allEntries=true)
	public void evictCache() {
		
	}
}
