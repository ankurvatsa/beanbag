package com.blockstein.beanbag.services;

import org.springframework.stereotype.Service;

@Service
public class WelcomeService implements IWelcomeService {
	public String retrieveWelcomeMessage() {
		return "Welcome to your beanbag!";
	}
}
