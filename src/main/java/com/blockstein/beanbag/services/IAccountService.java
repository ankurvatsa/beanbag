package com.blockstein.beanbag.services;

import com.blockstein.beanbag.models.Account;

public interface IAccountService {

	Account findByUsername(String username);

}
