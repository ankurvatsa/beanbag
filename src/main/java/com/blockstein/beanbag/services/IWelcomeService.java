package com.blockstein.beanbag.services;

public interface IWelcomeService {
	String retrieveWelcomeMessage();
}
