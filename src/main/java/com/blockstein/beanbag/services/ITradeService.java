package com.blockstein.beanbag.services;

import java.util.List;

import com.blockstein.beanbag.models.Trade;

public interface ITradeService {

	List<Trade> getAllLiveTrades();
	
	Trade getLiveTrade(Long tradeId);
	
	List<Trade> getAllTradeVersions(Long tradeId);
	
	Long addTrade(Trade trade);
	
	Long updateTrade(Trade trade, Long tradeId);
	
	void deleteTrade(Trade trade);
	
	void evictCache();
}
