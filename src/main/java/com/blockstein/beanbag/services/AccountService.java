package com.blockstein.beanbag.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blockstein.beanbag.models.Account;
import com.blockstein.beanbag.models.repositories.IAccountRepository;

@Service
public class AccountService implements IAccountService {
	
	@Autowired
	private IAccountRepository repository;

	@Override
	public Account findByUsername(String username) {
		return repository.findByUsername(username);
	}

}
