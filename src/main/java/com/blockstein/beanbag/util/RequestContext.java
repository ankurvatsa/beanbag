package com.blockstein.beanbag.util;

public class RequestContext {
	
	private static ThreadLocal<String> usernames = new ThreadLocal<String>();
	
	private RequestContext() {
		
	}
	
	public static String getUsername() {
		return usernames.get();
	}
	
	public static void setUsername(String username) { 
		usernames.set(username);
	}
	
	public static void init() {
		usernames.set(null);
	}
}
