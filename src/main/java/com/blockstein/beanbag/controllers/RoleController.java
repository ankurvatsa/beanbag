package com.blockstein.beanbag.controllers;

import java.util.Collection;
import java.util.Date;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.blockstein.beanbag.models.Role;
import com.blockstein.beanbag.models.repositories.RoleRepository;

public class RoleController extends BaseController {
	
	@Autowired
	private RoleRepository repository;
	
	@RequestMapping(
			value = "/api/v1/roles", 
			method = RequestMethod.GET, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<Role>> getRoles(@RequestParam(
			value = "queryType",
			defaultValue = "annotation") String queryType) {
		Collection<Role> roles = null;
		
		if(queryType.equals("method")) {
			DateTime now = new DateTime();
			roles = repository.findByEffectiveAtBeforeAndExpiresAtAfterOrExpiresAtNullOrderByOrdinalAsc(now, now);
		}
		else {
			roles = repository.findAllEffective(new DateTime());
		}
		
		return new ResponseEntity<Collection<Role>>(roles, HttpStatus.OK);
	}
}
