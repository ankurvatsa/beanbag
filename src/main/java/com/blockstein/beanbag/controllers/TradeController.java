package com.blockstein.beanbag.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.blockstein.beanbag.models.Trade;
import com.blockstein.beanbag.services.ITradeService;

@RestController
public class TradeController extends BaseController {
	@Autowired
	private ITradeService tradeService;
	
	@RequestMapping(method=RequestMethod.GET, value="/api/v1/trades")
	public List<Trade> getAllLiveTrades() {
		return tradeService.getAllLiveTrades();
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/api/v1/alltrades/{tradeId}")
	public List<Trade> getAllTradeVersions(@PathVariable Long tradeId) {
		return tradeService.getAllTradeVersions(tradeId);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/api/v1/trade/{tradeId}")
	public Trade getLiveTrade(@PathVariable Long tradeId) {
		return tradeService.getLiveTrade(tradeId);
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/api/v1/trades")
	public Long addTrade(@RequestBody Trade trade) {
		return tradeService.addTrade(trade);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/api/v1/trade/{tradeId}")
	public Long updateTrade(@RequestBody Trade trade, @PathVariable Long tradeId) {
		return tradeService.updateTrade(trade, tradeId);
	}
}
