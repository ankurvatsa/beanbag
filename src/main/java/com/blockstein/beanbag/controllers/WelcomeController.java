package com.blockstein.beanbag.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.blockstein.beanbag.services.IWelcomeService;

@RestController
public class WelcomeController extends BaseController {
	
	@Autowired
	private IWelcomeService welcomeService;
	
	@RequestMapping("/welcome")
	public String welcome() {
		return welcomeService.retrieveWelcomeMessage();
	}
}


