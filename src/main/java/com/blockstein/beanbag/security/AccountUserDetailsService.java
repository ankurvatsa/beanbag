package com.blockstein.beanbag.security;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;


import com.blockstein.beanbag.models.Account;
import com.blockstein.beanbag.models.Role;
import com.blockstein.beanbag.services.IAccountService;

@Service
public class AccountUserDetailsService implements UserDetailsService {
	
	@Autowired
	private IAccountService service;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Account account = service.findByUsername(username);
		if(account == null) {
			return null;
		}
		
		Collection<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
		for(Role role : account.getRoles()) {
			grantedAuthorities.add(new SimpleGrantedAuthority(role.getCode()));
		}
		
		User userDetails = new User(account.getUsername(), 
				account.getPassword(), 
				account.isEnabled(), 
				account.isExpired(), 
				account.isCredentialsExpired(), 
				account.isLocked(), 
				grantedAuthorities);
		
		return userDetails;
	}

}
