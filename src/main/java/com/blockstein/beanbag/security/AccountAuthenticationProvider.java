package com.blockstein.beanbag.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.blockstein.beanbag.util.RequestContext;

@Component
public class AccountAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {
	
	@Autowired
	private AccountUserDetailsService service;
	
	@Autowired
	private BCryptPasswordEncoder encoder;
	
	@Override
	protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken token) throws AuthenticationException {
		
		if((token.getCredentials() == null) || (userDetails.getPassword() == null)) {
			throw new BadCredentialsException("Credentials may not be null");
		}
				
		if(!encoder.matches((String) token.getCredentials(), userDetails.getPassword())) {
			throw new BadCredentialsException("Invalid Credentials");
		}
		
		RequestContext.setUsername(userDetails.getUsername());
		
	}
	@Override
	protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken token) throws AuthenticationException {
		
		UserDetails userDetails = service.loadUserByUsername(username);
		return userDetails;
	}
}
