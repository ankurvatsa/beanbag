package com.blockstein.beanbag.services;

import java.util.Collection;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.blockstein.beanbag.AbstractTest;
import com.blockstein.beanbag.models.Trade;

@Transactional
public class TradeServiceTest extends AbstractTest {
	
	@Autowired
	private ITradeService tradeService;
	
	@Before
	public void setup() {
		tradeService.evictCache();
	}
	
	@After
	public void tearDown() {
		
	}
	
	@Test
	public void testAddTrade() {
		Trade trade = new Trade();

		Integer buyCcy = 1;
		Double buyCcyAmt = 100.00;
		Integer sellCcy = 2;
		Double sellCcyAmt = 200.00;
		Double sellToBuyRate = sellCcyAmt/buyCcyAmt;
		Integer orderType = 1;
		Integer tradeDirection = 1;
		Integer tradeStatus = 1;
		
		trade.setId(1453L);
		trade.setBuyCcy(buyCcy);
		trade.setBuyCcyAmt(buyCcyAmt);
		trade.setSellCcy(sellCcy);
		trade.setSellCcyAmt(sellCcyAmt);
		trade.setSellToBuyRate(sellToBuyRate);
		trade.setOrderType(orderType);
		trade.setTradeDirection(tradeDirection);
		trade.setTradeStatus(tradeStatus);
		
		//TradeKey tkPostDbSave = tradeService.addTrade(trade);
		
		//Assert.assertEquals("Failure: Expected", tradeKey, tkPostDbSave);	
	}
	
	@Test
	public void testUpdateTrade() {
		// Only an existing, active trade can be amended
		Integer newBuyCcy=5;
		
		Trade trade = tradeService.getLiveTrade(1453L);
		Assert.assertNotNull("Failure: Expected valid trade, fetched null", trade);
		
		// Amend the trade's buy currency
		trade.setBuyCcy(newBuyCcy);
		tradeService.updateTrade(trade, trade.getId());
		
	}
	
	@Test
	public void testGetAllLiveTrades() {
		Collection<Trade> trades = tradeService.getAllLiveTrades();
		
		Assert.assertNotNull("Failure: Expected not null", trades);
		Assert.assertEquals("Failure: Expected size", 2, trades.size());
	}
	
	@Test
	public void testGetLiveTrade() {
		Long tradeId = 1453L;
		
		Trade liveTrade = tradeService.getLiveTrade(tradeId);
		
		Assert.assertNotNull("Failure: Expected non-null, live trade but fetched null", liveTrade);
		Assert.assertEquals("Failure: Expected live trade record to be fetched, but fetched", 1, (int) liveTrade.getTradeStatus());
	}
}