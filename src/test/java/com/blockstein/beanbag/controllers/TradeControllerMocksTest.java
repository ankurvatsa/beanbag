package com.blockstein.beanbag.controllers;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.blockstein.beanbag.AbstractControllerTest;
import com.blockstein.beanbag.models.Trade;
import com.blockstein.beanbag.services.ITradeService;

@Transactional
public class TradeControllerMocksTest extends AbstractControllerTest {
	
	@Mock
	private ITradeService tradeService;
	
	@InjectMocks
	private TradeController tradeController;
	
	private List<Trade> getEntityListStubData() {
		List<Trade> trades = new ArrayList<Trade>();
		trades.add(getEntityStubData());
		return trades;
	}
	
	private Trade getEntityStubData() {
		Trade trade = new Trade();

		trade.setId(16L);
		trade.setBuyCcy(1);
		trade.setBuyCcyAmt(100.001);
		trade.setOrderType(2);
		trade.setSellCcy(2);
		trade.setSellCcyAmt(200.002);
		trade.setSellToBuyRate(2.000);
		trade.setTradeDirection(2);
		trade.setTradeStatus(1);
		trade.setCreatedBy("user");
		
		return trade;
	}

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		
		setUp(tradeController);
	}
	
	@Test
	public void testGetAllTrades() throws Exception {
		
		List<Trade> trades = getEntityListStubData();
		
		when(tradeService.getAllLiveTrades()).thenReturn(trades);
		
		String uri = "/api/v1/trades";
		
		MvcResult result = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON)).andReturn();
		
		String content = result.getResponse().getContentAsString();
		int status = result.getResponse().getStatus();
		
		verify(tradeService, times(1)).getAllLiveTrades();
		
		Assert.assertEquals("Failure: Expected HTTP status 200", 200, status);
		Assert.assertEquals("Failure: Expected HTTP response body to have a value", content.trim().length() > 0);
	}
	
	@Test
	public void testGetTrade() {
		Trade trade = getEntityStubData();
		Long tradeId = 1098L;
		
		String uri = "/api/v1/trade" + tradeId;
	}
	
	@Test
	public void testAddTrade() {
		String uri = "/api/v1/trades";		
		Trade trade = getEntityStubData();
	}
	
	@Test 
	public void testUpdateTrade() {
		String uri = "/api/v1/trade";		
	}
}
