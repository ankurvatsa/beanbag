package com.blockstein.beanbag.controllers;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.blockstein.beanbag.AbstractControllerTest;
import com.blockstein.beanbag.services.ITradeService;

import org.junit.Assert;

@Transactional
public class TradeControllerTest extends AbstractControllerTest {
	
	@Autowired
	private ITradeService tradeService;
	
	@Before
	public void setUp() {
		super.setUp();
		tradeService.evictCache();
	}
	
	@Test
	public void testGetAllTrades() throws Exception {
		
		String uri = "/trades";
		
		MvcResult result = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON)).andReturn();
		
		String content = result.getResponse().getContentAsString();
		int status = result.getResponse().getStatus();
		
		Assert.assertEquals("Failure: Expected HTTP status 200", 200, status);
		Assert.assertEquals("Failure: Expected HTTP response body to have a value", content.trim().length() > 0);
	}
	
	@Test
	public void testGetTrade() throws Exception {
		String uri = "/trade/";
		
		MvcResult result = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON)).andReturn();
		
		String content = result.getResponse().getContentAsString();
		int status = result.getResponse().getStatus();
		
		Assert.assertEquals("Failure: Expected HTTP status 200", 200, status);
		Assert.assertEquals("Failure: Expected HTTP response body to have a value", content.trim().length() > 0);
		
		// TODO
		// Only active trades should be returned
	}
	
	@Test
	public void testAddTrade()  {
		String uri = "/trades";
	}
	
	@Test
	public void testUpdateTrade() {
		String uri = "/trade";
	}
}