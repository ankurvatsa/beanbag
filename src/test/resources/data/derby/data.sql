insert into DTDTradeDetails (
  DTDTradeId INT
, DTDTradeVersion INT
, DTDTradeUser INT
, DTDTradeDirection INT
, DTDSellCcy INT
, DTDSellCcyAmt DECIMAL(12, 4)
, DTDBuyCcy INT
, DTDBuyCcyAmt DECIMAL(12, 4)
, DTDCTimeStamp TIMESTAMP
, DTDOrderType int
, DTDSellToBuyRate DECIMAL(12, 4)
, DTDTradeStatus int
)
values
(
  1
, 1
, 1
, 1
, 1
, 1.0
, 1
, 1.0
, CURRENT_TIMESTAMP
, 1
, 1.0
, 1
)
;

INSERT INTO Greeting (text) values ('Hello, World!');
INSERT INTO Greeting (text) values ('What up?');

INSERT INTO Account (referenceId, username, password, enabled, expired, locked, credentialsExpired, version, createdBy, createdAt, updatedBy, updatedAt) values ('ECAFA1F8B94C4DC6A0E0B23ACBA2AEE3', 'user', 'user', true, false, false, false, 0, 'user', NOW(), NULL, NULL);
INSERT INTO Account (referenceId, username, password, enabled, expired, locked, credentialsExpired, version, createdBy, createdAt, updatedBy, updatedAt) values ('D7AEE52A42BC46C89C51D62794F1A196', 'operations', 'operations', true, false, false, false, 0, 'user', NOW(), NULL, NULL);

INSERT INTO Role (id, code, label, ordinal, effectiveAt, expiresAt, createdAt) values (1, 'ROLE_USER', 'User', 0, '2015-01-01 00:00:00', NULL, NOW());
INSERT INTO Role (id, code, label, ordinal, effectiveAt, expiresAt, createdAt) values (2, 'ROLE_ADMIN', 'Administrator', 0, '2015-01-01 00:00:00', NULL, NOW());
INSERT INTO Role (id, code, label, ordinal, effectiveAt, expiresAt, createdAt) values (3, 'ROLE_SYSADMIN', 'System Administrator', 0, '2015-01-01 00:00:00', NULL, NOW());

INSERT INTO AccountRole (accountId, roleId) Select a.id, r.id from Account a, Role r where a.username = 'user' and r.id = 1;
INSERT INTO AccountRole (accountId, roleId) Select a.id, r.id from Account a, Role r where a.username = 'operations' and r.id = 3;
